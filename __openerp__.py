# -*- coding: utf-8 -*-
{
    'name': 'Module Name',
    'version': '1.0',
    'category': 'Category',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Subtitle',
    'images': [],
    'depends': [
        'base',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/ir_sample_view',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}
